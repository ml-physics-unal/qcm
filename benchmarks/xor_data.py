from sklearn.datasets import make_blobs
from sklearn.preprocessing import MinMaxScaler
import matplotlib.pyplot as plt
import numpy as np
from qcm.circuit import ProjectionCircuit
from qcm.qfm import SinCos, Softmax

# Create XOR data:
std = 0.2
n_samples = 100
data = []
labels = []
for i in range(4):
    fi, si = divmod(i, 2)
    blob, _ = make_blobs(
        n_samples=n_samples, centers=np.array([[fi, si]]), cluster_std=std
    )
    data.append(blob)
    labels.append(np.zeros(n_samples) if fi == si else np.ones(n_samples))
data = np.vstack(data)
labels = np.hstack(labels)

scaler = MinMaxScaler()
data = scaler.fit_transform(data)

colours = np.array(["r", "b"])
plt.scatter(*data.T, c=colours[labels.astype("int")])
plt.show()

# Define quantum measurement classification objects
# 1st the quantum feature map
qfm = SinCos()
# 2nd the projection circuit classifier
proj = ProjectionCircuit(qfm)

# Fit the classifier (this doesn't learn anything!)
proj.fit(data, labels.astype(int))

# Predict some data:
xnew = np.array([0.8, 0.8])
print(proj.predict(xnew))

# Or a batch of data:
xnew = np.array([[0.2, 0.2], [0.2, 0.8]])
print(proj.predict(xnew))

# Let's do the same with another quantum feature map
qfm = Softmax(4, 10)
proj = ProjectionCircuit(qfm)
proj.fit(data, labels.astype(int))

# Predict some data:
xnew = np.array([0.8, 0.8])
print(proj.predict(xnew))

# Or a batch of data:
xnew = np.array([[0.2, 0.2], [0.2, 0.8]])
print(proj.predict(xnew))
