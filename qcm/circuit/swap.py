import numpy as np
from qiskit import QuantumCircuit, execute
from qiskit.extensions import Initialize
from qiskit.quantum_info import Statevector
from typeguard import typechecked

from . import AbstractCircuitClassifier


# TODO: adapt all this file to the new abstract circuit classifier proposal
class SwapCircuit(AbstractCircuitClassifier):
    """
    Creates a SwapCircuit object with a quantum feature map.
    The circuit consists of three steps:

    1. A circuit that fits some data.
    2. A circuit that fits a new data sample.
    3. A circuit that performs a SWAP test between the first and
    second circuits., measuring the ancilla qubit and the class qubit.

    Parameters
    ----------
    qfm: qcm.qfm.QuantumFeatureMap
        A quantum feature map from the qcm package

    Notes
    -----
    The circuit is a qubit circuit.
    Note that if the ancilla is measured at 0 (meaning that the states
    passed the test), it does not imply that the states are equal.
    For completely orthogonal states, the probability of a pair of
    1 qubit states passing the test is 1/2.
    """

    @typechecked
    def _set_n_qubits(self, x_features: int):
        """
        Private method to set the number of qubits in the circuit.

        Parameters
        ----------
        x_features: int
            The number of coordinates of the data samples (only the X part).

        Notes
        -----
        The number of qubits is `2 * x_features * self._qfm.n_qubits_x` for
        storing the train and prediction states in the X part. 1 qubit is
        needed for the label, and another as an ancilla qubit for the SWAP
        test.
        """
        self._n_qubits = 2 * x_features * self._qfm.n_qubits_x + 2

    @typechecked
    def _predict_sample(self, sample: np.ndarray, shots: int = 1024):
        """
        Predicts a single sample.
        """
        if self._n_qubits is None:
            raise AttributeError(
                "The circuit has not been fit. Run the `fit` method before predicting"
            )
        assert sample.ndim == 1
        n_qubits_sample_x = int((self._n_qubits - 2) / 2)
        assert abs(sample.size - (n_qubits_sample_x / self._qfm.n_qubits_x)) < 1e-5
        predict_state = self._qfm.qmapX(sample)
        predict_state = Initialize(
            Statevector(predict_state.full().flatten().tolist())
        ).gates_to_uncompute()
        self._qc = QuantumCircuit(self._n_qubits, 2)
        self._qc.append(self._fit_block, range(n_qubits_sample_x + 1))
        self._qc.append(
            predict_state, range(n_qubits_sample_x + 1, 2 * n_qubits_sample_x + 1)
        )
        self._qc.barrier()
        ancilla = self._n_qubits - 1
        self._qc.h(ancilla)
        for qubit in range(1, 1 + n_qubits_sample_x):
            self._qc.cx(qubit, qubit + n_qubits_sample_x)
            self._qc.ccx(ancilla, qubit + n_qubits_sample_x, qubit)
            self._qc.cx(qubit, qubit + n_qubits_sample_x)
        self._qc.h(ancilla)
        self._qc.barrier()
        self._qc.measure(0, 0)  # pylint: disable=no-member
        self._qc.measure(ancilla, 1)  # pylint: disable=no-member

        results = execute(self._qc, self._backend, shots=shots).result().get_counts()
        counts0 = None
        counts1 = None
        success = 0
        for bitstring, counts in results.items():
            if bitstring.startswith("0"):
                success += counts
                pred = bitstring[-1]
                if pred == "0":
                    counts0 = counts
                else:
                    counts1 = counts
        if counts0 is not None or counts1 is not None:
            if counts1 is None:
                prob1 = 0
            elif counts0 is None:
                prob1 = 1
            else:
                prob1 = counts1 / (counts1 + counts0)
        else:
            prob1 = 0.5
        label = round(prob1)
        return label, prob1, success / sum(results.values())


class DestructiveSwapCircuit(AbstractCircuitClassifier):
    """
    Creates a DestructiveSwapCircuit object with a quantum feature map.
    The circuit consists of three steps:

    1. A circuit that fits some data.
    2. A circuit that fits a new data sample.
    3. A circuit that performs a destructive SWAP test between the first and
    second circuits, measuring all qubits to perform a bitwise AND between the X
    qubits.

    Parameters
    ----------
    qfm: qcm.qfm.QuantumFeatureMap
        A quantum feature map from the qcm package

    Notes
    -----
    The circuit is a qubit circuit.
    Note that if the test passed, it does not imply that the states are equal.
    For completely orthogonal states, the probability of a pair of
    1 qubit states passing the test is 1/2.
    """

    @typechecked
    def _set_n_qubits(self, x_features: int):
        """
        Private method to set the number of qubits in the circuit.

        Parameters
        ----------
        x_features: int
            The number of coordinates of the data samples (only the X part).

        Notes
        -----
        The number of qubits is `2 * x_features * self._qfm.n_qubits_x` for
        storing the train and prediction states in the X part. 1 additional
        qubit is needed for the label.
        """
        self._n_qubits = 2 * x_features * self._qfm.n_qubits_x + 1

    def _check_bitstring(self, bitstring, n_qubits_sample_x):
        label = bitstring[-1]
        ϕ = bitstring[:n_qubits_sample_x]
        ψ = bitstring[n_qubits_sample_x : 2 * n_qubits_sample_x]
        # Flips are even -> passes
        flips = bin(int(ϕ, 2) & int(ψ, 2)).count("1")
        if flips % 2 == 0:
            return True, label
        else:
            return False, label

        # # Checking that the bitstrings are the same
        # if ϕ != ψ:
        #     return False, label
        # else:
        #     return True, label

    @typechecked
    def _predict_sample(self, sample: np.ndarray, shots: int = 1024):
        """
        Predicts a single sample.
        """
        if self._n_qubits is None:
            raise AttributeError(
                "The circuit has not been fit. Run the `fit` method before predicting"
            )
        assert sample.ndim == 1
        n_qubits_sample_x = int((self._n_qubits - 1) / 2)
        assert abs(sample.size - (n_qubits_sample_x / self._qfm.n_qubits_x)) < 1e-5
        predict_state = self._qfm.qmapX(sample)
        predict_state = Initialize(
            Statevector(predict_state.full().flatten().tolist())
        ).gates_to_uncompute()
        self._qc = QuantumCircuit(self._n_qubits, self._n_qubits)
        self._qc.append(self._fit_block, range(n_qubits_sample_x + 1))
        self._qc.append(
            predict_state, range(n_qubits_sample_x + 1, 2 * n_qubits_sample_x + 1)
        )
        self._qc.barrier()
        for q in range(1, n_qubits_sample_x + 1):
            self._qc.cx(q, q + n_qubits_sample_x)
            self._qc.h(q)
        self._qc.barrier()
        for q in range(self._n_qubits):
            self._qc.measure(q, q)  # pylint: disable=no-member

        results = execute(self._qc, self._backend, shots=shots).result().get_counts()
        counts0 = 0
        counts1 = 0
        success = 0
        for bitstring, counts in results.items():
            passed, pred = self._check_bitstring(bitstring, n_qubits_sample_x)
            if passed:
                success += counts
                if pred == "0":
                    counts0 += counts
                else:
                    counts1 += counts
        if counts0 != 0 or counts1 != 0:
            if counts1 == 0:
                prob1 = 0
            elif counts0 == 0:
                prob1 = 1
            else:
                prob1 = counts1 / (counts1 + counts0)
        else:
            prob1 = 0.5
        label = round(prob1)
        return label, prob1, success / sum(results.values())
