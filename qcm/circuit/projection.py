import time

import numpy as np
from qiskit import QuantumCircuit, execute
from qiskit.compiler import transpile
from qiskit.providers.ibmq import IBMQBackend
from scipy.spatial.distance import hamming
from typeguard import typechecked

from ..result import Result
from . import AbstractCircuitClassifier, AbstractCircuitDensityEstimator
from .abstract_circuit import AbstractCircuit


class ProjectionCircuit(AbstractCircuit):
    """
    Creates a ProjectionCircuit object with a quantum feature map.
    The circuit consists of three steps:

    1. A circuit that fits some data.
    2. A circuit that projects data onto the state of a new sample.
    3. A circuit that measures in the computational basis.

    Parameters
    ----------
    qfm: qcm.qfm.QuantumFeatureMap
        A quantum feature map from the qcm package

    Notes
    -----
    The circuit is a qubit circuit.
    """

    def _send_job(self, sample, shots):
        assert sample.ndim == 1
        assert (
            abs(
                sample.size
                - (self._n_qubits - self.start_X_qubit) / self._qfm.n_qubits_x
            )
            < 1e-5
        )
        sample_inverse_circuit = self._circuit_qfm_map(sample)
        self._qc = QuantumCircuit(self._n_qubits, self._n_qubits)
        self._qc.append(self._fit_block, range(self._n_qubits))
        self._qc.barrier()
        self._qc.append(
            sample_inverse_circuit,
            range(self.start_X_qubit, self._n_qubits),
        )
        self._qc.barrier()
        for q in range(self._n_qubits):
            self._qc.measure(q, q)  # pylint: disable=no-member
        if isinstance(self._backend, IBMQBackend):
            transpiled = transpile(self._qc, backend=self._backend)
            job = self._backend.run(transpiled, shots=shots)
        else:
            job = execute(
                self._qc, self._backend, shots=shots, **self._backend_noise_kwargs
            )
        result = Result(job)
        self._session.new_result(result)
        return job

    def _result_is_ready(self, result: Result):
        return result.info is not None

    def _process_job(self, job, timeout=5):
        t = 0
        sleep_time = 0.1
        while not job.done() and t < timeout:
            time.sleep(sleep_time)
            t += sleep_time
            try:
                job.refresh()
            except Exception:
                pass
        if not job.done():
            raise Exception(f"Job {job.job_id()} has status {job.status()}")
        self._session.new_result(Result(job))
        return self._session(job.job_id())


class ClassificationProjectionCircuit(ProjectionCircuit, AbstractCircuitClassifier):
    start_X_qubit = 1

    @typechecked
    def _set_n_qubits(self, x_features: int):
        """
        Private method to set the number of qubits in the circuit.

        Parameters
        ----------
        x_features: int
            The number of coordinates of the data samples (only the X part).
        """
        self._n_qubits = x_features * self._qfm.n_qubits_x + 1

    def _process_result(
        self,
        result: Result,
        sample_size: int,
        post_process: bool = False,
        power: int = 10,
    ):
        if not self._result_is_ready(result):
            return None
        binary_counts = result.info["binary_counts"]
        success = 0
        bitstring_dicts = (
            {}
        )  # {bitstring : {"counts0": num, "counts1": num, "similitude": sim}}
        for bitstring, counts in binary_counts.items():
            xbitstring = bitstring[: self._qfm.n_qubits_x * sample_size]
            if xbitstring not in bitstring_dicts.keys():
                bitstring_dicts[xbitstring] = {
                    "counts0": 0,
                    "counts1": 0,
                    "similitude": 0,
                }
            if bitstring.startswith("0" * self._qfm.n_qubits_x * sample_size):
                bitstring_dicts[xbitstring]["similitude"] = 1
                success += counts
                # TODO: at the moment only one qubit is used for prediction, but in the future more can be used!
                pred = bitstring[-1]
                if pred == "0":
                    bitstring_dicts[xbitstring]["counts0"] = counts
                else:
                    bitstring_dicts[xbitstring]["counts1"] = counts
                continue
            if post_process:
                d = hamming(
                    [int(bit) for bit in bitstring[:-1]],
                    [0] * self._qfm.n_qubits_x * sample_size,
                )
                s = (1 / (1 + d)) ** power
                bitstring_dicts[xbitstring]["similitude"] = s
                pred = bitstring[-1]
                if pred == "0":
                    bitstring_dicts[xbitstring]["counts0"] = counts
                else:
                    bitstring_dicts[xbitstring]["counts1"] = counts
        weights = []
        probs = []
        for count_dict in bitstring_dicts.values():
            weights.append(count_dict["similitude"])
            probs.append(self._get_prob(count_dict["counts0"], count_dict["counts1"]))
        prob1 = np.dot(weights, probs) / sum(weights)
        label = round(prob1)
        return label, prob1, success / sum(binary_counts.values())

    def _get_prob(self, counts0, counts1):
        if counts0 != 0 or counts1 != 0:
            if counts1 == 0:
                prob1 = 0
            elif counts0 == 0:
                prob1 = 1
            else:
                prob1 = counts1 / (counts1 + counts0)
        else:
            prob1 = 0.5
        return prob1


class DensityEstimationProjectionCircuit(
    ProjectionCircuit, AbstractCircuitDensityEstimator
):
    start_X_qubit = 0

    @typechecked
    def _set_n_qubits(self, x_features: int):
        """
        Private method to set the number of qubits in the circuit.

        Parameters
        ----------
        x_features: int
            The number of coordinates of the data samples (only the X part).
        """
        if self._qfm.n_qubits_individual_feature:
            self._n_qubits = x_features * self._qfm.n_qubits_x
        else:
            self._n_qubits = self._qfm._total_num_qubits

    def _process_result(
        self,
        result: Result,
        sample_size: int,
    ):
        if not self._result_is_ready(result):
            return None
        results = result.info["binary_counts"]
        count0 = 0
        total = 0
        for bitstring, counts in results.items():
            if bitstring.startswith("0" * self._qfm.n_qubits_x * sample_size):
                count0 += counts
            total += counts
        return count0 / total, count0
