import abc
from typing import Callable, Iterable, Optional, Union

import numpy as np
from qiskit import IBMQ, Aer
from qiskit.circuit import QuantumCircuit
from qiskit.extensions import Initialize
from qiskit.providers import BackendV1 as Backend
from qiskit.providers.ibmq import IBMQBackend, least_busy
from qiskit.quantum_info import Statevector
from qiskit_aer.noise import NoiseModel
from typeguard import typechecked

from ..qfm import QuantumFeatureMap
from ..session import Session


class AbstractCircuit(abc.ABC):
    """
    Class for qubit circuits to perform machine learning tasks with
    quantum circuit measurements.
    """

    @typechecked
    def __init__(
        self,
        qfm: QuantumFeatureMap,
        backend: str = "qasm_simulator",
        noise_model: Optional[str] = None,
        token: Optional[str] = None,
        session: Session = Session(),
        circuit_qfm_map: Optional[Callable[[np.ndarray], QuantumCircuit]] = None,
    ):
        self._qfm: QuantumFeatureMap = qfm
        self._fit_block = None
        self._qc = None
        self._n_qubits: Optional[int] = None
        self._backend: Backend = self._set_backend(backend, token)
        self._session = session
        print(self._backend_info(self._backend))
        self._backend_n_qubits: int = self._backend.configuration().n_qubits
        self._backend_noise_kwargs = {}
        if noise_model is not None:
            assert backend == "qasm_simulator", (
                "If you want to simulate a noisy quantum circuit, use qasm_simulator as"
                " the backend"
            )
            noise_backend = self._set_backend(noise_model, token)
            noise_model = NoiseModel.from_backend(noise_backend)
            coupling_map = noise_backend.configuration().coupling_map
            basis_gates = noise_model.basis_gates
            self._backend_noise_kwargs = {
                "noise_model": noise_model,
                "coupling_map": coupling_map,
                "basis_gates": basis_gates,
            }
        if circuit_qfm_map is None:

            def circuit_qfm_map_fn(sample):
                predict_state = self._qfm.qmapX(sample)
                predict_state = Initialize(
                    Statevector(predict_state.full().flatten().tolist())
                )
                return predict_state.gates_to_uncompute()

            self._circuit_qfm_map = circuit_qfm_map_fn
        else:
            self._circuit_qfm_map = circuit_qfm_map

    def _set_backend(self, backend: str, token: Optional[str] = None) -> Backend:
        if backend == "qasm_simulator":
            return Aer.get_backend("qasm_simulator")
        else:
            if IBMQ.active_account() is None:
                IBMQ.enable_account(token)
            provider = IBMQ.get_provider(hub="ibm-q")
            if backend == "least_busy":
                return least_busy(provider.backends())
            else:
                return provider.get_backend(backend)

    def _backend_info(self, backend: Backend):
        output = f"Name:\t{backend.name()}\n"
        output += f"Status:\t{backend.status()}\n"
        output += f"Configuration:\n{backend.configuration()}"
        return output

    @abc.abstractmethod
    def _set_n_qubits(self, x_features):
        raise NotImplementedError

    @abc.abstractmethod
    def _send_job(self, sample, shots):
        raise NotImplementedError

    @abc.abstractmethod
    def _process_job(self, job):
        raise NotImplementedError

    @abc.abstractmethod
    def _process_result(self, result, sample_size, *args):
        raise NotImplementedError

    @property
    def n_qubits(self):
        return self._n_qubits


class AbstractCircuitClassifier(AbstractCircuit):
    """
    Class for qubit circuits to perform quantum measurement classification.
    """

    @typechecked
    def fit(self, X: np.ndarray, y: Iterable[Union[int, np.int64, np.int32]]):
        """
        Creates the state of the data (X, y), and the corresponding
        quantum circuit to initialise a circuit in that state.

        Parameters
        ----------
        X: np.ndarray of shape (M, N)
            Data input of M samples and N features.
        y: List[int] of length M
            The labels of each row in X.
        """
        if X.ndim != 2:
            raise ValueError(f"X must be 2-dimensional and it has {X.ndim} dimensions")
        train_state = self._qfm.qmap(X, y)
        train_state = (
            Initialize(Statevector(train_state.full().flatten().tolist()))
            .gates_to_uncompute()
            .inverse()
        )
        self._fit_block = train_state
        self._set_n_qubits(X.shape[1])

    @typechecked
    def predict(self, X: np.ndarray, shots: int = 1024):
        """
        Creates a circuit to perform a prediction onto an input matrix.

        Parameters
        ----------
        X: np.ndarray of shape (S, N) or (N,)
            Data input to be predicted, containing S samples of N
            features, or just 1 sample of N features

        Returns
        -------
        np.ndarray of shape (S,) or int if S=1
            The predicted output label.
        np.ndarray of shape (S,) or float if S=1
            The probability that each sample **belongs to the label 1**.
        np.ndarray of shape (S,) or float if S=1
            The percentage of shots that were useful to compute the
            probability of the label. Some of the shots do not collapse
            the prediction state to the train state, so they are useless
            to compute the probability of the label.
        """
        if isinstance(self._backend, IBMQBackend):
            raise AttributeError(
                "You cannot directly predict using an IBMQ Backend "
                "Please use `backend='qasm_simulator'` to directly predict "
                "or instead use the `send_jobs` method, and then "
                "`process_jobs`."
            )

        if self._n_qubits is None:
            raise AttributeError(
                "The circuit has not been fit. Run the `fit` method before predicting"
            )

        assert X.ndim == 1 or X.ndim == 2
        if X.ndim == 1:
            X = X.reshape(1, -1)
        jobs = self.send_jobs(X, shots)
        results = self.process_jobs(jobs)
        return self.process_results(results, X.shape[1])

    def send_jobs(self, samples, shots: int = 1024):
        jobs = [self._send_job(sample, shots) for sample in samples]
        self._session.checkpoint(jobs, [{"sample": sample} for sample in samples])
        return jobs

    def process_jobs(self, jobs):
        results = [self._process_job(job) for job in jobs]
        self._session.checkpoint()
        return results

    def process_results(self, results, sample_size, post_process=False, power=1):
        S = len(results)
        labels = np.empty((S,))
        probs = np.empty((S,))
        successes = np.empty((S,))
        for i, result in enumerate(results):
            output = self._process_result(result, sample_size, post_process, power)
            if output is not None:
                labels[i] = output[0]
                probs[i] = output[1]
                successes[i] = output[2]
            else:
                labels[i] = np.nan
                probs[i] = np.nan
                successes[i] = np.nan
        return labels, probs, successes


class AbstractCircuitDensityEstimator(AbstractCircuit):
    @typechecked
    def fit(self, X: np.ndarray):
        """
        Creates the state of the data X, and the corresponding
        quantum circuit to initialise a circuit in that state.

        Parameters
        ----------
        X: np.ndarray of shape (M, N)
            Data input of M samples and N features.
        """
        if X.ndim != 2:
            raise ValueError(f"X must be 2-dimensional and it has {X.ndim} dimensions")
        train_state = sum(self._qfm.qmapX(x) for x in X).unit()
        train_state = (
            Initialize(Statevector(train_state.full().flatten().tolist()))
            .gates_to_uncompute()
            .inverse()
        )
        self._fit_block = train_state
        self._set_n_qubits(X.shape[1])

    @typechecked
    def predict(self, X: np.ndarray, shots: int = 1024):
        """
        Creates a circuit to perform a prediction onto an input matrix.

        Parameters
        ----------
        X: np.ndarray of shape (S, N) or (N,)
            Data input to be predicted, containing S samples of N
            features, or just 1 sample of N features

        Returns
        -------
        np.ndarray of shape (S,) or float if S=1
            The probability density of each sample.
        np.ndarray of shape (S,) or int if S=1
            The number of counts of the 0 bit string.
        """
        if isinstance(self._backend, IBMQBackend):
            raise AttributeError(
                "You cannot directly predict using an IBMQ Backend "
                "Please use `backend='qasm_simulator'` to directly predict "
                "or instead use the `send_jobs` method, and then "
                "`process_jobs`."
            )

        if self._n_qubits is None:
            raise AttributeError(
                "The circuit has not been fit. Run the `fit` method before predicting"
            )

        assert X.ndim == 1 or X.ndim == 2
        if X.ndim == 1:
            X = X.reshape(1, -1)
        jobs = self.send_jobs(X, shots)
        results = self.process_jobs(jobs)
        return self.process_results(results, X.shape[1])

    def send_jobs(self, samples, shots: int = 1024):
        jobs = [self._send_job(sample, shots) for sample in samples]
        self._session.checkpoint()
        return jobs

    def process_jobs(self, jobs):
        results = [self._process_job(job) for job in jobs]
        self._session.checkpoint()
        return results

    def process_results(self, results, sample_size):
        S = len(results)
        probs = np.empty((S,))
        counts = np.empty((S,))
        for i, result in enumerate(results):
            probs[i], counts[i] = self._process_result(result, sample_size)
        return probs, counts
