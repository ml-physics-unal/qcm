import jax.numpy as jnp
import numpy as np
import tensorcircuit as tc
K = tc.set_backend("jax")

def data_layer(
    circuit: tc.Circuit,
    data_vec: jnp.ndarray,
    n_qubits: int
)   -> tc.Circuit:
    """
    Circuit for data encoding.

    Args:
        circuit (Circuit): Circuit class of tensorcircuit
        data_vec (ndarray): Vector where n times the float to 
            encode is stored
        n_qubits (int): Quantum feature map encoding 
            qubits (n)

    Returns:
        Circuit: Data encoding circuit with the float to be 
            encoded placed in the rotation gates  
    """
    for m in range(2):
        for i in range(n_qubits):
            circuit.ry(i, theta = data_vec[0])
        if n_qubits == 2:
            circuit.cnot(0, 1)
        else:
            for i in range(circuit._nqubits):
                circuit.cnot(i, (i + 1) % circuit._nqubits)
        for i in range(n_qubits):
            circuit.rx(i, theta = data_vec[0])

def variational_layer(
    n_qubits, 
    circuit: tc.Circuit, 
    params: jnp.ndarray
)   -> tc.Circuit:
    """
    Variational circuit.

    Args:
        n_qubits (int): Quantum feature map encoding 
            qubits (n)
        circuit (Circuit): Circuit class of tensorcircuit
        params (ndarray): List of real numbers where each 
            number is a rotation parameter associated 
                with each rotation quantum gate
    
    Returns:
        Circuit: Variational circuit with the parameter 
            vector placed in the rotation gates
    """
    iterador = 0
    for i in range(circuit._nqubits):
        circuit.ry(i, theta=params[iterador])
        iterador += 1
    if n_qubits == 2:
        circuit.cnot(0, 1)
    else:
        for i in range(circuit._nqubits):
            circuit.cnot(i, (i + 1) % circuit._nqubits) 
    for i in range(circuit._nqubits):
        circuit.rx(i, theta=params[iterador])
        iterador += 1
     
    
def ansatz(
    n_qubits: int, 
    n_layers: int
):
    """
    Ansatz circuit consisting of the data encoding 
        circuit and the variational circuit.

    Args:
        n_qubits (int): Quantum feature map encoding 
            qubits (n)
        n_layers (int): Number of consecutive repetitions 
            of the variational circuit

    Returns:
        Just in time compilation of the PQC.
    """

    def ansatz_embedding(
        params: jnp.ndarray,
        data_vec: jnp.ndarray
    )-> jnp.ndarray:
        """
        Placing of the float and parameters in the ansatz 
            circuit and measurement of the circuit final 
                state vector

        Args:
            params (ndarray): List of real numbers where each 
                number is a rotation parameter associated 
                    with each rotation quantum gate
            data_vec (ndarray): Vector where n times the float 
                to is stored

        Returns:
            ndarray: Circuit final state vector
        """
        n_params_per_layer = 2*n_qubits
        circuit = tc.Circuit(n_qubits)
        data_layer(circuit, data_vec, circuit._nqubits)
        for i in range(n_layers):
            variational_layer(
                n_qubits, circuit, 
                params[i*n_params_per_layer: (i + 1)*n_params_per_layer]
            )
        return circuit.wavefunction()

    return K.jit(ansatz_embedding)
