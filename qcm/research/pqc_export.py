from qiskit.circuit import QuantumCircuit
import numpy as np

def qiskit_trained_QFM(
    n_qubits: int,
    n_layers: int, 
    n_features: int, 
    data_to_map: np.ndarray, 
    best_params: np.ndarray
) -> QuantumCircuit:
    """
    Construction of the circuit that approximately prepares any quantum feature 
        map. The circuit is always trained with one feature, but here the circuit 
            is built to integrate any number of features.
    Args:
        n_qubits (int): Quantum feature map encoding qubits (n)
        n_layers (int): Number of consecutive repetitions of the 
            variational circuit
        n_features (int): Number of features to be mapped
        data_to_map (Array): Vector containing the floats (features) to be encoded
        best_params (Array): Best parameters obtained through ansatz training

    Returns:
        QuantumCircuit: Quantum circuit of the trained QFM exported in qiskit
    """
    
    qiskit_params = n_features*list(best_params)

    circuit = QuantumCircuit(n_features*n_qubits)

    for f in range(n_features):
        s = (f+1)*n_qubits
        for m in range(2):
            for i in range (f*n_qubits, s):
                circuit.ry(data_to_map[f], i)
            if n_qubits == 2:
                for i in range(f*n_qubits, s-1):
                    circuit.cnot(f*n_qubits, s-1)
            else: 
                for i in range(f*n_qubits, s):
                    if i == s-1:
                        circuit.cnot(i, i-(n_qubits-1))
                    else:
                        circuit.cnot(i, (i+1) % s)  
            for i in range (f*n_qubits, s):
                circuit.rx(data_to_map[f], i)

    param_count = 0
    for f in range(n_features):
        for layer in range(n_layers):
            s = (f+1)*n_qubits
            for i in range(f*n_qubits, s):
                circuit.ry(qiskit_params[param_count], i)
                param_count += 1
            if n_qubits == 2:
                for i in range(f*n_qubits, s-1):
                    circuit.cnot(f*n_qubits, s-1)
            else: 
                for i in range(f*n_qubits, s):
                    if i == s-1:
                        circuit.cnot(i, i-(n_qubits-1))
                    else:
                        circuit.cnot(i, (i+1) % s)
            for i in range(f*n_qubits, s):
                circuit.rx(qiskit_params[param_count], i)
                param_count += 1
            
    return circuit

