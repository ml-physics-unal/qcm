import jax
import jax.numpy as jnp
from scipy.stats import qmc
from tqdm import tqdm
import tensorcircuit as tc
K = tc.set_backend("jax")

def pseudo_loss(
    params: jnp.ndarray, 
    data_vec: jnp.ndarray,
    ansatz_circuit: tc.Circuit, 
    target_state: jnp.ndarray, 
) -> float:
    """
    Infidelity between the state vector prepared by the ansatz circuit and 
        the target state vector of the exact quantum feature map.
    
    Args:
        params (ndarray): List of real numbers where each number is a rotation 
            parameter associated with each rotation quantum gate
        data_vec (ndarray): Vector where n times the float to is stored
        ansatz_circuit (Circuit): Ansatz circuit consisting of the data encoding 
            circuit and the variational circuit.
        target_State: Target state vector of the exact quantum feature map

    Returns: 
        float: Infidelity between the prepared state vector and the target 
            state vector
    """
    circuit_state = ansatz_circuit(params, data_vec)
    return 1-jnp.abs(jnp.dot(circuit_state, target_state))**2


def pseudo_loss_vectorized(
    params: jnp.ndarray, 
    data_vec_list:  jnp.ndarray,
    ansatz_circuit: tc.Circuit,
    target_state_list: jnp.ndarray
) -> float:
    """
    Vectorization of the cost function (infidelity) so that it can be applied 
        to a list of prepared and target vectors in parallel. 
    
    Args:
        params (ndarray): List of real numbers where each number is a rotation 
            parameter associated with each rotation quantum gate
        data_vec_list (ndarray): List of vectors where each vector contains n 
            times a float to be encoded
        ansatz_circuit (Circuit): Ansatz circuit consisting of the data encoding 
            circuit and the variational circuit.
        target_state_list: List of target state vectors of the exact quantum 
            feature map for each float to be encoded

    Returns:
        float: Mean infidelity between the prepared state vector list and the 
            target state vector list
    """
    pseudo_loss_vmap = jax.vmap(pseudo_loss, (None, 0, None, 0), 0)
    return jnp.mean(pseudo_loss_vmap(
        params, 
        data_vec_list,
        ansatz_circuit, 
        target_state_list)
    )


def sobol_search(
    data_vec_list: jnp.ndarray, 
    target_state_list: jnp.ndarray, 
    n_qubits: int, 
    n_layers: int, 
    ansatz_circuit: tc.Circuit,
    sobol_points: int, 
    sobol_interval: float, 
) -> jnp.ndarray:
    """
    Best parameters search by Sobol sampling of points in the parameter space.
    
    Args: 
        data_vec_list (ndarray): List of vectors where each vector contains 
            n times a float to be encoded
        target_state_list: List of target state vectors of the exact quantum 
            feature map for each float to be encoded
        n_qubits (int): Quantum feature map encoding qubits (n)
        n_layers (int): Number of consecutive repetitions of the 
            variational circuit
        ansatz_circuit (Circuit): Ansatz circuit consisting of the data encoding 
            circuit and the variational circuit
        sobol_points (int): Number of points to sample 
        sobol_interval (float): Half of the lateral side associated with the 
            hypercube where the points are sampled.
    Returns:
        Array: Vector of the best parameters found with 
            Sobol sampling
        
    """
    sampler = qmc.LatinHypercube(d = n_layers*2*n_qubits)
    sample = sampler.random(n = sobol_points)
    params_sample = qmc.scale(sample, -sobol_interval, sobol_interval)

    losses_sample = []

    for params in tqdm(params_sample):
        loss_sample = pseudo_loss_vectorized(
            params, 
            data_vec_list,
            ansatz_circuit, 
            target_state_list
        )
        losses_sample.append(loss_sample)

    return params_sample[losses_sample.index(min(losses_sample))]


def ansatz_training(
    data_vec_list: jnp.ndarray,
    target_state_list: jnp.ndarray,
    n_qubits: int, 
    n_layers: int,
    ansatz_circuit: tc.Circuit,
    sobol_points: int, 
    sobol_interval: float,
    rate: float,
    min_delta: float, 
    patience: int
):
    """
    Training the ansatz to approximately prepare any quantum feature map. 
        The training is based on minimizing the average infidelity of many 
            samples; best parameters are searched with Sobol sampling and 
                then a gradient descent refinement.
    
    Args:
        data_vec_list (ndarray): List of vectors where each vector contains 
            n times a float to be encoded
        target_state_list: List of target state vectors of the exact quantum 
            feature map for each float to be encoded
        n_qubits (int): Quantum feature map encoding qubits (n)
        n_layers (int): Number of consecutive repetitions of the 
            variational circuit
        ansatz_circuit (Circuit): Ansatz circuit consisting of the data encoding 
            circuit and the variational circuit
        sobol_points (int): Number of points to sample 
        sobol_interval (float): Half of the lateral side associated with the 
            hypercube where the points are sampled.
        rate (float): Gradient learning rate
        min_delta: Minimal improvement in cost minimization so that after the 
            number of patience iterations has passed the algorithm stops
        patience (float): Number of iterations to achieve min_delta 

    Returns:
        [dic, Array]: [Training results, best parameters vector]
    """
    circuit = ansatz_circuit (n_qubits, n_layers)

    def loss(
        params
    ):
        return pseudo_loss_vectorized(
            params, 
            data_vec_list,
            circuit, 
            target_state_list
        )
        
    params = sobol_search(
        data_vec_list,
        target_state_list,
        n_qubits, 
        n_layers, 
        circuit, 
        sobol_points,
        sobol_interval, 
    )
        
    loss_and_grad = K.jit(K.value_and_grad(loss))
    losses = []

    best_loss = 10000
    n_iterations_without_improvement = 0


    for i in tqdm(range(5000)):
        loss_value, grad = loss_and_grad(params)
        losses.append(loss_value)
        params -= rate * grad

        improvement = best_loss - loss_value

        if improvement > min_delta:
            best_loss = loss_value
            n_iterations_without_improvement = 0
        else:
            n_iterations_without_improvement += 1
        if n_iterations_without_improvement > patience:
            break
        
    results_dic = {
        "Number_of_Qubits": n_qubits,
        "Number_of_Layers": n_layers,
        "Number_of_Parameters": 2*n_qubits*n_layers,
        "Learning_rate": rate,
        "Min_Delta": min_delta,
        "Patience_Number": patience,
        "Number of Steps": i,
        "Final_Loss": float(loss_value),
    }

    return [results_dic, params]

