from typing import Optional, Union

from qiskit.providers.ibmq import IBMQJob
from qiskit_aer.jobs import AerJob


class Result:
    """
    This dataclass handles a single sample result.
    """

    def __init__(self, job: Union[AerJob, IBMQJob], metadata: Optional[dict] = None):
        self.id = job.job_id()
        if job.done():
            result = job.result()
            self.info = {**result.to_dict(), "binary_counts": result.get_counts()}
        else:
            self.info = None
        self.metadata = metadata

    def __repr__(self):
        return f"Result({self.to_dict()})"

    def to_dict(self):
        return {"id": self.id, "info": self.info, "metadata": self.metadata}
