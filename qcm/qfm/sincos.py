from . import QuantumFeatureMap
from typeguard import typechecked
from qutip import basis, tensor
import numpy as np


class SinCos(QuantumFeatureMap):
    def __init__(self):
        super(SinCos, self).__init__()
        self.dimx = 2
        self.n_qubits_x = 1

    @typechecked
    def _qmapX_float(self, x: float):
        """
        Maps a float to a qubit state

        .. math::\sin(x\pi)|0\rangle + \cos(x\pi)|1\rangle

        Parameters
        ----------
        x: float
            A float that must be between 0 and 1.

        Returns
        -------
        qutip.qobj.Qobj
            A qubit state.
        """
        assert x >= 0 and x <= 1
        return np.sin(np.pi * x) * self.ket0 + np.cos(np.pi * x) * self.ket1
