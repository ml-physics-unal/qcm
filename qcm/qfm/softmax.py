from . import QuantumFeatureMap
from typeguard import typechecked
from qutip import basis, tensor
import numpy as np
from typing import Union, Iterable
import abc
from collections import deque


class Softmax(QuantumFeatureMap):
    """
    Softmax quantum feature map

    Parameters
    ----------
    αs: int or List[float]
        If this is an int, each component of the input will be
        expressed as the state of an αs-level system, where
        each level refers to "how close" is the component of
        the input to some equidistantly-distributed centers in
        the range [0, 1]. If it is a list, each list member is
        the coordinate of a center. Therefore, passing a list
        allows for non-equidistant centers.
    """

    _kets = {"0": QuantumFeatureMap.ket0, "1": QuantumFeatureMap.ket1}

    @typechecked
    def __init__(self, αs: Union[int, Iterable[float]], β: float = 10):
        super(Softmax, self).__init__()
        if isinstance(αs, int):
            self.dimx = αs
            self._αs = np.linspace(0, 1, αs)
        else:
            self.dimx = len(αs)
            self._αs = np.asarray(αs)
        self.n_qubits_x = None
        self._β = β

    @typechecked
    def _qmapX_float(self, x: float):
        """
        Maps a float to a state of qubits.
        First, there is a calculation of the following probabilities

        .. math::P_i(x) = \text{softmax}(\exp(-β|x - α_i|^2))

        where :math:`α_i` is the ith element of the centers
        `self._αs`. If there are m such centers, the state of the map is

        .. math:: \sum_{j=1}^m \sqrt{P_i(x)}|j\rangle,

        where :math:`|j\rangle` is the state of an m-level system.
        The state of this m-level system is mapped to a qubits state
        through a method `_nlevels2qubits`. Note that by construction
        the state is normalised.

        Parameters
        ----------
        x: float
            A float that must be between 0 and 1.

        Returns
        -------
        qutip.qobj.Qobj
            A qubit state.
        """
        assert x >= 0 and x <= 1
        Pvec = np.exp(-self._β * (x - self._αs) ** 2)
        Pvec = Pvec / np.sum(Pvec)
        return sum(np.sqrt(Pvec[j]) * self._nlevels2qubits(j) for j in range(self.dimx))

    @abc.abstractmethod
    def _nlevels2qubits(self, level):
        return NotImplementedError


class SoftmaxBinary(Softmax):
    """
    Creates a quantum feature map that performs a map between n-level
    systems and qubits through a binary map, i.e. it writes the state
    :math:`|n\rangle` as binary through :math:`n \to \text{binary}(n)`.
    """

    def __init__(self, αs, β=10):
        super(SoftmaxBinary, self).__init__(αs, β)
        self.n_qubits_x = int(np.floor(np.log2(self.dimx) + 1))

    @typechecked
    def _nlevels2qubits(self, level: int):
        return tensor(*(self._kets[k] for k in f"{level:b}".zfill(self.n_qubits_x)))


class SoftmaxOneHot(Softmax):
    """
    Creates a quantum feature map that performs a map between n-level
    systems and qubits through a one-hot encoding, i.e. it writes the
    state :math:`|n\rangle` in a system with n-1 qubits doing one-hot.
    For instance, let's consider a system of 5 levels. We will need 4
    qubits to map these. The map of each level is:

    - :math:`|0\rangle \to |0,0,0,0\rangle`
    - :math:`|1\rangle \to |0,0,0,1\rangle`
    - :math:`|2\rangle \to |0,0,1,0\rangle`
    - :math:`|3\rangle \to |0,1,0,0\rangle`
    - :math:`|4\rangle \to |1,0,0,0\rangle`
    """

    def __init__(self, αs, β=10):
        super(SoftmaxOneHot, self).__init__(αs, β)
        self.n_qubits_x = self.dimx - 1

    @typechecked
    def _nlevels2qubits(self, level: int):
        single_states = deque([self._kets["0"]] * self.n_qubits_x + [self._kets["1"]])
        single_states.rotate(-level)
        return tensor(*list(single_states)[: self.n_qubits_x])
