import abc
from typing import Iterable, Union

import numpy as np
from qutip import basis, tensor
from typeguard import typechecked


class QuantumFeatureMap(abc.ABC):
    ket0 = basis(2, 0)
    ket1 = basis(2, 1)

    def __init__(self):
        self.dim_x = None
        self.dim_y = 2
        self.n_qubits_x = None
        self.n_qubits_y = 1
        self.n_qubits_individual_feature = True
        self._total_num_qubits = None

    @typechecked
    def qmapX(self, x: np.ndarray):
        """
        Maps the single-sample vector `x` to a quantum state.

        Parameters
        ----------
        x: np.ndarray of shape (N,)
            An N-dimensional vector

        Returns
        -------
        qutip.qobj.Qobj
            The quantum state corresponding to the input vector `x`.
        """
        assert x.ndim == 1
        return tensor(*(self._qmapX_float(xi) for xi in x)).unit()

    @abc.abstractmethod
    def _qmapX_float(self, x: float):
        """
        Maps a component of the input vector `x` to a quantum state.
        """
        return NotImplementedError

    @typechecked
    def qmapY(self, y: Union[int, np.int64, np.int32]):
        """
        Maps a single label `y` to a quantum state.

        Parameters
        ----------
        y: int
            Either 0 or 1 for currently supported binary labels.

        Returns
        -------
        qutip.qobj.Qobj
            The quantum state corresponding to the label `y`.

        Notes
        -----
        Currently only binary labels are supported. In the future
        multi-labels will be supported.
        """
        assert y in [0, 1]
        return basis(2, y)

    @typechecked
    def qmap(
        self,
        x: np.ndarray,
        y: Union[np.int64, np.int32, int, Iterable[Union[int, np.int64, np.int32]]],
    ):
        """
        Maps the intput-output pair (x, y) to a quantum state.

        Parameters
        ----------
        x: np.ndarray of shape (N,) or shape (M, N)
            An N-dimensional vector with the input data if it is
            a single sample, or M, N-dimensional vectors if there
            are M samples.
        y: int or Iterable[int]
            Either 0 or 1 for currently supported binary labels if
            it is an int. If it is a list, it must have M elements
            that are either 0 or 1.

        Returns
        -------
        qutip.qobj.Qobj
            The quantum state corresponding to the tensor product
            of the `x` quantum state and the `y` quantum state if
            they are a single sample, or the sum of quantum states
            corresponding to pairs of (x, y)

        Notes
        -----
        Currently only binary labels are supported. In the future
        multi-labels will be supported.
        """
        if x.ndim == 1:
            assert isinstance(y, int)
            state = self._qmap_sample(x, y)
        elif x.ndim == 2:
            assert len(y) == x.shape[0]
            state = sum(self._qmap_sample(x[i], y[i]) for i in range(len(y)))
        return state.unit()

    @typechecked
    def _qmap_sample(self, x: np.ndarray, y: Union[int, np.int64, np.int32]):
        """
        Quantum feature maps a single sample.
        """
        return tensor(self.qmapX(x), self.qmapY(y))
