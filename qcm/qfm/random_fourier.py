from . import QuantumFeatureMap
from typeguard import typechecked
from qutip import basis, tensor
import numpy as np
from typing import Union, Iterable
import abc
from collections import deque
from sklearn.kernel_approximation import RBFSampler

class RFF(QuantumFeatureMap):
    _kets = {"0": QuantumFeatureMap.ket0, "1": QuantumFeatureMap.ket1}

    @typechecked
    def __init__(self, rff: RBFSampler):
        super().__init__()
        self.n_qubits_x = None
        self._rff = rff
        self.dimx = rff.n_components
        self.n_qubits_individual_feature = False

    def _qmapX_float(self, x):
        pass

    def qmapX(self, x):
        assert x.ndim == 1
        rffs = self._rff.transform(x.reshape(1, -1)).flatten()
        return sum(rffs[j] * self._nlevels2qubits(j) for j in range(self.dimx)).unit()
    
    @abc.abstractmethod
    def _nlevels2qubits(self, level):
        return NotImplementedError
    
class RFFBinary(RFF):
    def __init__(self, rff: RBFSampler):
        super().__init__(rff)
        self.n_qubits_x = int(np.ceil(np.log2(self.dimx)))
        self._total_num_qubits = self.n_qubits_x

    def _nlevels2qubits(self, level: int):
        return tensor(*(self._kets[k] for k in f"{level:b}".zfill(self.n_qubits_x)))

class RFFOneHot(RFF):
    def __init__(self, rff: RBFSampler):
        super().__init__(rff)
        self.n_qubits_x = self.dimx - 1
        self._total_num_qubits = self.n_qubits_x

    def _nlevels2qubits(self, level: int):
        single_states = deque([self._kets["0"]] * self.n_qubits_x + [self._kets["1"]])
        single_states.rotate(-level)
        return tensor(*list(single_states)[: self.n_qubits_x])