from typing import Callable

import numpy as np
from qutip import Qobj
from typeguard import typechecked

from . import QuantumFeatureMap

class Coherent(QuantumFeatureMap):
    def __init__(
        self,
        number_of_qubits: int,
        gamma: float,
        theta_scaler: Callable[[float], float],
    ):
        super().__init__()
        self.gamma = gamma
        self.theta_scaler = theta_scaler
        self.n_qubits_x = number_of_qubits

    @typechecked
    def _qmapX_float(self, x) -> Qobj:
        """
        Mapping of a real number into a truncated coherent state represented by means
        of N qubits.

        Args:
            x_i (float): Real number to encode

        Return:
            Qobj: Qobj of n complex numbers where each is the i-th amplitude associated
            with the i-th state up to n (n=2^N). This object represents the N qubit
            state.
        """
        theta = self.theta_scaler(x)
        alpha = x * np.exp(1j * theta)

        coherent = np.asarray(
            [
                np.exp(-(self.gamma * np.abs(alpha) ** 2) / 2)
                * ((alpha**i) * (self.gamma ** (i / 2)))
                / (np.prod(np.sqrt(np.arange(1, i + 1))))
                for i in range(2**self.n_qubits_x)
            ]
        )

        coherent /= np.linalg.norm(coherent)

        return Qobj(
            coherent.reshape(-1, 1),
            dims=[[2**self.n_qubits_x], [1]],
            shape=(2**self.n_qubits_x, 1),
        )
