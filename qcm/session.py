import pickle
from collections import OrderedDict
from typing import List, Literal, Optional

from qiskit_aer.jobs import AerJob

from .result import Result


class Session:
    def __init__(
        self,
        results: List[Result] = [],
        filename: str = "default_session.pickle",
        mode: Literal["w", "a"] = "w",
    ):
        self.results = OrderedDict()
        for result in results:
            self.results[result.id] = result
        self.filename = filename
        self.mode = mode

    def new_result(self, result: Result):
        self.results[result.id] = result

    def checkpoint(
        self,
        new_jobs: Optional[List[AerJob]] = None,
        metadatas: Optional[List[dict]] = None,
    ):
        if new_jobs is None:
            new_jobs = []
        if metadatas is None:
            metadatas = [{}] * len(new_jobs)
        assert len(new_jobs) == len(metadatas)
        for job, metadata in zip(new_jobs, metadatas):
            result = Result(job, metadata)
            self.new_result(result)
        with open(self.filename, f"{self.mode}b") as f:
            pickle.dump(self.results.copy(), f)

    def load(self, backend):
        # Load the results of the previous session
        with open(self.filename, "rb") as f:
            old_results = pickle.load(f)
        for _id, result in old_results.items():
            # If a job doesn't have info, load it with the Result interface, which will
            # check if the job is done and retrieve the info
            if result.info is None:
                job = backend.retrieve_job(_id)
                metadata = result.metadata
                result = Result(job, metadata)
                old_results[_id] = result
        # Assign to this session the results of the previous session
        self.results = old_results
        self.checkpoint()

    def __repr__(self):
        return f"Session({self.results})"

    def __call__(self, id):
        if id in self.results.keys():
            return self.results[id]
        else:
            raise ValueError("Invalid id")
