import numpy as np
from sklearn.datasets import make_blobs
from sklearn.preprocessing import MinMaxScaler


def make_xor(n_samples, std, random_state=0, feature_range=(0, 1)):
    data = []
    labels = []
    for i in range(4):
        fi, si = divmod(i, 2)
        blob, _ = make_blobs(
            n_samples=n_samples,
            centers=np.array([[fi, si]]),
            cluster_std=std,
            random_state=random_state,
        )
        random_state += 1
        data.append(blob)
        labels.append(np.zeros(n_samples) if fi == si else np.ones(n_samples))
    data = np.vstack(data)
    labels = np.hstack(labels)

    scaler = MinMaxScaler(feature_range=feature_range)
    data = scaler.fit_transform(data)
    return data, labels.astype("int")
