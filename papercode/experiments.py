import pickle

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from make_xor import make_xor
from mpl_toolkits.axes_grid1 import make_axes_locatable
from qiskit import QuantumCircuit
from scipy.spatial import KDTree
from scipy.stats import bernoulli, norm
from sklearn.datasets import make_circles, make_moons
from sklearn.preprocessing import MinMaxScaler
from tqdm import tqdm

from qcm.circuit import (
    ClassificationProjectionCircuit,
    DensityEstimationProjectionCircuit,
)


class Mixture(object):
    def __init__(self, loc1=0, scale1=1, loc2=1, scale2=1, alpha=0.5):
        self.var1 = norm(loc=loc1, scale=scale1)
        self.var2 = norm(loc=loc2, scale=scale2)
        self.alpha = alpha

    def pdf(self, x):
        return self.alpha * self.var1.pdf(x) + (1 - self.alpha) * self.var2.pdf(x)

    def rvs(self, size=1):
        vals = np.stack([self.var1.rvs(size), self.var2.rvs(size)], axis=-1)
        idx = bernoulli.rvs(1.0 - self.alpha, size=size, random_state=0)
        return np.array([vals[i, idx[i]] for i in range(size)])


def make_1d_gaussians(num, show=False):
    from sklearn.preprocessing import MinMaxScaler

    mixt = Mixture(loc1=0, scale1=0.5, loc2=3, scale2=1, alpha=0.5)
    np.random.seed(seed=233423)
    data = mixt.rvs(num)
    scaler = MinMaxScaler(feature_range=(0.2, 0.8))

    if show:
        fig, ax = plt.subplots()
        minimum = -4.5
        maximum = 6.5
        x = np.linspace(minimum, maximum, 100)
        ax.plot(x, mixt.pdf(x), label="true")
        sns.kdeplot(data, ax=ax, label="kde")
        sns.rugplot(data, ax=ax)
        plt.legend()
        plt.show()
    return scaler.fit_transform(data.reshape(-1, 1))


def experiment_density(data, circuit, gridsize=20, save_path=None, show=False):
    from scipy.integrate import simps

    grid1d = np.linspace(0, 1, gridsize)
    circuit.fit(data)
    density, counts = circuit.predict(grid1d.reshape(-1, 1))
    density /= simps(density, dx=grid1d[1] - grid1d[0])
    if show:
        fig, ax = plt.subplots()
        plt.plot(grid1d, density, label="dmkde")
        sns.kdeplot(data.flatten(), ax=ax, label="kde")
        sns.rugplot(data.flatten(), ax=ax)
        plt.legend()
        plt.show()
        print(density)


def experiment(
    data, labels, circuit, gridsize=20, save_path=None, show=False, feature_range=(0, 1)
):
    grid1d = np.linspace(*feature_range, gridsize)
    xx, yy = np.meshgrid(grid1d, grid1d)
    D = np.empty(xx.shape)
    Z = np.empty(xx.shape)
    S = np.empty(xx.shape)
    circuit.fit(data, labels)
    for i, x1 in tqdm(enumerate(grid1d), total=gridsize):
        for j, x2 in enumerate(grid1d):
            label, prob, succ = circuit.predict(np.array([x1, x2]))
            D[i, j] = label
            Z[i, j] = prob
            S[i, j] = succ
    print("Experiment was successful")
    if isinstance(save_path, str):
        with open(save_path, "wb") as f:
            pickle.dump([D, Z, S], f)

    if show:
        fig, axes = plt.subplots(ncols=3, figsize=(20, 6))
        axes[0].set_title("Label")
        axes[1].set_title("Probability")
        axes[2].set_title("Success ratio")

        c0 = axes[0].contourf(
            xx, yy, D, levels=40, cmap="RdBu", alpha=0.5, antialiased=True
        )
        c1 = axes[1].contourf(xx, yy, Z, levels=40, cmap="RdBu")
        axes[1].contour(xx, yy, Z, levels=[0.5])
        c2 = axes[2].contourf(xx, yy, S, levels=40, cmap="YlGn")

        for ax, c in zip(axes, [c0, c1, c2]):
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("right", size="5%", pad=0.05)
            fig.colorbar(c, cax=cax, orientation="vertical")

        colours = np.array(["r", "b"])
        for ax in axes:
            ax.scatter(*data.T, c=colours[labels], alpha=0.5)
        plt.show()
    return xx, yy, D, Z, S


def predict_in_grid(
    sample: np.ndarray,
    points: np.ndarray,
    probs: np.ndarray,
    neighbours: int,
    tree: KDTree = None,
):
    """
    Predicts a point given a grid of points and its associated probabilities.

    Parameters
    ----------
    sample : np.ndarray
        Single data point to predict with shape (D,)
    points : np.ndarray
        An array with shape (N, D)
    probs : np.ndarray
        An array with probabilities with shape (N,)
    neighbours : int
        Number of neighbours to look for.
    tree : KDTree, optional.
        A KDTree which is optiional. Default is None
    """
    if tree is None:
        tree = KDTree(points)

    distances, indices = tree.query(sample, k=neighbours)
    near_probs = probs[indices]

    similitudes = 1 / (1 + distances)
    return np.sum(similitudes * near_probs) / np.sum(similitudes), tree


def classification(
    qfm_hyp,
    qfm_select,
    qfm_trained,
    grid_size,
    xor_points,
    xor_std,
    n_qubits,
    n_layers,
    n_features,
):
    from qcm.qfm import Coherent, SoftmaxOneHot

    def data_to_circuit(data_point: np.ndarray) -> QuantumCircuit:
        import os
        from pathlib import Path

        from qcm.research.pqc_export import qiskit_trained_QFM

        with open(
            os.path.join(
                Path(__file__).resolve().parent,
                "Coherent_{}_{}_{}.npy".format(n_qubits, n_layers, n_features),
            ),
            "rb",
        ) as f:
            best_params = np.load(f)

        qiskit_circuit_to_encode = qiskit_trained_QFM(
            n_qubits, n_layers, n_features, data_point, best_params
        )

        return qiskit_circuit_to_encode

    if qfm_select == "Coherent":
        qfm = Coherent(
            number_of_qubits=n_qubits, gamma=qfm_hyp, theta_scaler=lambda x: x * np.pi
        )
    elif qfm_select == "SoftmaxOneHot":
        qfm = SoftmaxOneHot(n_qubits + 1, qfm_hyp)
    else:
        raise ValueError("qfm_select must be Coherent or SoftmaxOneHot")

    if qfm_trained:
        circ = ClassificationProjectionCircuit(qfm, circuit_qfm_map=data_to_circuit)
    else:
        circ = ClassificationProjectionCircuit(qfm, circuit_qfm_map=None)

    feature_range = (0, 1)
    X, y = make_xor(xor_points, xor_std, feature_range=feature_range)

    experiment(
        X,
        y,
        circ,
        gridsize=grid_size,
        save_path=None,
        show=True,
        feature_range=feature_range,
    )


def density():
    from sklearn.kernel_approximation import RBFSampler

    from qcm.qfm import Coherent, RFFBinary

    X = make_1d_gaussians(100, show=False)
    # rff = RBFSampler(gamma=80, n_components=8)
    # rff.fit(X)
    # qfm = RFFBinary(rff)
    # qfm = RFFOneHot(rff)
    # qfm = SoftmaxOneHot(np.linspace(-1, 2, 7), 10)
    # qfm = SoftmaxBinary(np.linspace(-1, 2, 64), 100)
    qfm = Coherent(2, 100, lambda x: x * np.pi)
    circ = DensityEstimationProjectionCircuit(qfm)
    experiment_density(X, circ, gridsize=100, show=True)


if __name__ == "__main__":
    # classification(
    #     qfm_hyp=9.6,
    #     qfm_select="Coherent",
    #     qfm_trained=False,
    #     grid_size=30,
    #     xor_points=250,
    #     xor_std=0.2,
    #     n_qubits=2,
    #     n_layers=3,
    #     n_features=2,
    # )
    density()
