from qcm.qfm import SinCos, SoftmaxBinary, SoftmaxOneHot
import numpy as np
from qutip import tensor
from sklearn.preprocessing import MinMaxScaler

X0 = np.random.randn(1, 3)
y0 = 0
X1 = np.random.randn(20, 3)
y1 = [1] * 20
scaler = MinMaxScaler()
X0 = scaler.fit_transform(X0).squeeze()
X1 = scaler.fit_transform(X1)


def try_qfm_works(qfm):
    x0map = qfm.qmapX(X0)
    y0map = qfm.qmapY(y0)
    try:
        qfm.qmapX(X1)
    except AssertionError:
        pass
    try:
        qfm.qmapY(y1)
    except TypeError:
        pass
    x0y0map = qfm.qmap(X0, y0)
    x1y1map = qfm.qmap(X1, y1)

    assert x0y0map.shape == x1y1map.shape
    assert tensor(x0map, y0map) == x0y0map
    assert abs(x0map.norm() - 1) < 1e-6
    assert abs(y0map.norm() - 1) < 1e-6
    assert abs(x0y0map.norm() - 1) < 1e-6
    assert abs(x1y1map.norm() - 1) < 1e-6


def test_sincos():
    qfm = SinCos()
    try_qfm_works(qfm)


def test_softmax():
    qfm = SoftmaxBinary(2, 10)
    try_qfm_works(qfm)
    qfm = SoftmaxBinary(5, 10)
    try_qfm_works(qfm)
    qfm = SoftmaxBinary(np.array([0, 0.1, 0.5, 0.9]), 10)
    try_qfm_works(qfm)
    qfm = SoftmaxOneHot(2, 10)
    try_qfm_works(qfm)
    qfm = SoftmaxOneHot(5, 10)
    try_qfm_works(qfm)
    qfm = SoftmaxOneHot(np.array([0, 0.1, 0.5, 0.9]), 10)
    try_qfm_works(qfm)
