import os
import sys
from pathlib import Path

import jax.numpy as jnp
import numpy as np
import optuna
from optuna import Trial
from sklearn.model_selection import train_test_split

from qcm.circuit import ClassificationProjectionCircuit
from qcm.qfm import Coherent, SoftmaxOneHot
from qcm.research.pqc_export import qiskit_trained_QFM
from qcm.research.pqc_training import ansatz_training

sys.path.append(os.path.join(Path(__file__).resolve().parent.parent, "papercode"))
from make_xor import make_xor


def circuit_training(
    qfm_hyp,
    qfm_select,
    n_qubits,
    n_layers,
    ansatz_circuit,
    sobol_points,
    sobol_interval,
    training_points,
    rate,
    min_delta,
    patience,
):
    training_set = np.random.uniform(0, 1, size=(training_points, 1))

    if qfm_select == "Coherent":
        qfm = Coherent(n_qubits, qfm_hyp, lambda x: x * np.pi)
    elif qfm_select == "SoftmaxOneHot":
        qfm = SoftmaxOneHot(n_qubits + 1, qfm_hyp)

    target_state = []
    for vec in training_set:
        x_m = qfm.qmapX(vec).full().flatten()
        target_state.append(x_m)

    target_state = jnp.asarray(target_state)

    dic, best_params = ansatz_training(
        training_set,
        target_state,
        n_qubits,
        n_layers,
        ansatz_circuit,
        sobol_points,
        sobol_interval,
        rate,
        min_delta,
        patience,
    )

    print(dic)

    return [dic, np.array(best_params)]


def cost(y_pred: np.ndarray, y_true: np.ndarray) -> float:
    """
    Negative log likelihood of the true labels under the predicted

    Args:
        y_pred (npt.NDArray): Logits (probability that the label is 1)
        y_true (npt.NDArray): True labels (0 or 1)

    Returns:
        float: negative log-likehood
    """
    return -float(np.mean(y_true * np.log(y_pred) + (1 - y_true) * np.log(1 - y_pred)))


def classification_opt(
    n_qubits,
    qfm_select,
    circuit_qfm_map,
    xor_points,
    xor_std,
    n_trials,
    min_qfm_hyp,
    max_qfm_hyp,
):
    def qfm_hyp_sugg(trial: Trial) -> dict:
        return {"qfm_hyp": trial.suggest_float("qfm_hyp", min_qfm_hyp, max_qfm_hyp)}

    def qfm_hyp_obj(suggestions):
        data, labels = make_xor(
            xor_points, xor_std, random_state=0, feature_range=(0, 1)
        )

        X_train, X_test, y_train, y_test = train_test_split(
            data, labels, random_state=0, test_size=0.3
        )

        if qfm_select == "Coherent":
            qfm = Coherent(n_qubits, suggestions["qfm_hyp"], lambda x: x * np.pi)
        elif qfm_select == "SoftmaxOneHot":
            qfm = SoftmaxOneHot(n_qubits + 1, suggestions["qfm_hyp"])

        circuit = ClassificationProjectionCircuit(qfm, circuit_qfm_map=circuit_qfm_map)

        circuit.fit(X_train, y_train)
        pred_labels, pred_probs, pred_shots = circuit.predict(X_test)

        return cost(pred_probs, y_test)

    def optuna_float_training(trial):
        return qfm_hyp_obj(qfm_hyp_sugg(trial))

    study = optuna.create_study()
    study.optimize(optuna_float_training, n_trials=n_trials)
    # df = study.trials_dataframe()

    return study.best_params["qfm_hyp"]


""" if __name__ == "__main__":
    # hyp search for exact qfm
    exact_qfm_hyp = classification_opt(
        n_qubits = 2,
        qfm_select = 'Coherent', 
        circuit_qfm_map = None,  
        xor_points = 250, 
        xor_std = 0.2, 
        n_trials = 50,
        min_qfm_hyp = 9,
        max_qfm_hyp = 10
    )

    print(exact_qfm_hyp) """

if __name__ == "__main__":
    # qfm training
    import matplotlib.pyplot as plt

    from qcm.research.pqc import ansatz

    n_qubits = 2
    n_layers = 4
    n_features = 2
    qfm_select = "Coherent"

    dic, best_params = circuit_training(
        qfm_hyp=9.6,
        qfm_select=qfm_select,
        n_qubits=n_qubits,
        n_layers=n_layers,
        ansatz_circuit=ansatz,
        sobol_points=5000,
        sobol_interval=np.pi,
        training_points=60_000,
        rate=0.5,
        min_delta=0.01,
        patience=500,
    )

    print(best_params)

    qiskit_circuit_to_encode = qiskit_trained_QFM(
        n_qubits=n_qubits,
        n_layers=n_layers,
        n_features=n_features,
        data_to_map=np.array([0.2, 0.5]),
        best_params=best_params,
    )

    qiskit_circuit_to_encode.draw("mpl")
    plt.show()

    np.save(
        "{}_{}_{}_{}".format(qfm_select, n_qubits, n_layers, n_features), best_params
    )
