from setuptools import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="qcm",
    packages=["qcm", "qcm.circuit", "qcm.qfm", "qcm.research"],
    version="0.1.0",
    description="Library for machine learning tasks with quantum circuit measurements",
    long_description=long_description,
    long_description_content_type="text/markdown",
    author="Vladimir Vargas-Calderón, Fabio A. González and Juan Ardila",
    author_email="{vvargasc, fagonzalezo, juardilag}@unal.edu.co",
    license="GNUv3",
    install_requires=["typeguard>=2.12", "qiskit>=0.27"],
    python_requires=">=3.6",
)
